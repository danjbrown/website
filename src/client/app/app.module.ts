import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faTwitter, faGitlab, faLinkedin, faGithub, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { FooterComponent } from './footer/footer.component';
import { HttpClientModule } from '@angular/common/http';

library.add(faGitlab, faLinkedin, faGithub, faInstagram);

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
