/* global describe it before after */
'use strict';

const chai        = require('chai');
const chaiHttp    = require('chai-http');
const Server      = require('./server');
const User        = require('./user');
const should      = chai.should(); // eslint-disable-line no-unused-vars

chai.use(chaiHttp);

describe('User service', () => {
    let server;
    let service; // eslint-disable-line no-unused-vars

    before(() => {
        server = new Server();
        service = new User(server);
        server.start();
    });

    after(() => {
        server.stop();
    });

    it('it should get a list of users', (done) => {
        chai.request(server.app)
            .get('/services/users')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
});