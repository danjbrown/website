# Website

My personal website built using Angular together with web services running on a separate port.

The application is deployed using GitLab CI/CD and maintained in production using pm2.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.4.

## Installing and running the application

Clone and install the application
```
npm install
```

Run the client application on dev accessible at http://localhost:4200
```
npm start
```

Run the web services on dev/live accessible at http://localhost:3100/services
```
npm run server
```

Run the client and server linting
```
npm run lint:client
npm run lint:server
```

Run the client end-to-end tests via Protractor, client unit tests via Karma and the server tests via Mocha and Chai.
```
npm run test:client
npm run test:server
```

Build the client application. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
```
npm run build
```

Run the server for the built client application accessible at http://localhost:3000
```
npm run client
```

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).