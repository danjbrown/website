#!/bin/bash

# clone the repo
echo "Cloning application..."
git clone https://gitlab.com/danjbrown/website.git

## reinstall pm2 globally
##echo "Installing pm2..."
##npm remove pm2 -g
##npm install pm2 -g
##pm2 status

## npm install
echo "Installing application in install directory '/website'..."
cd website
npm install

echo "Installing express in install directory '/website'..."
npm install express

echo "Building application in install directory '/website'..."
node_modules/.bin/ng build --prod

echo "Copying application from install directory '/website' to live directory '/live/website'..."
cd ..
rm -rf live/website/*
cp -r  website/. live/website

echo "Removing install directory '/website'..."
rm -rf website

# start the app
echo "Restarting application..."
## NODE_ENV=live pm2 start live/website/src/server/app.js --name website
## NODE_ENV=live pm2 start live/website/src/server/index.js --name services
pm2 restart website
pm2 restart services