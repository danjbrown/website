import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Dan Brown :: Software Engineer';
  data = '';
  servicesBaseUrl = environment.servicesBaseUrl;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    /*this.http.get(this.servicesBaseUrl + '/users').subscribe(
      (result) => {
        console.log(result);
        this.data = result['data'];
      }
    );*/
  }
}
