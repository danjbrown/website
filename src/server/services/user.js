'use strict';

class User {
    constructor(server) {
        this.server = server;
        this.router = server.router;
        this.logger = server.logger;

        this.registerRoutes();
    }

    registerRoutes() {
        this.router.get('/services/users', this.getUsers.bind(this));
    }
    
    getUsers(req, res) {
        res.status(200).json({success: true, data:
            {
                users : [
                    {name: 'Mr Test User', id: 1234}
                ]
            }
        });
    }
}

module.exports = User;
