const express = require('express');
const compression = require('compression');
const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = 'info';

const app = express();
const host = 'localhost';
const port = 3000;

app.use(compression());

if (process.env.NODE_ENV === 'live') {
    app.use('/', express.static('live/website/dist'));
} else {
    app.use('/', express.static('dist'));
}

app.listen(port);

logger.info('Starting server at http://' + host + ':' + port);
